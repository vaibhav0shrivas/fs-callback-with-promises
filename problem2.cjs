
const fs = require("fs");
const path = require("path");

const deleteThisFile = function (filePath) {
  return new Promise((resolve, rejects) => {

    fs.unlink(filePath, (error) => {
      if (error) {

        console.error(`Failed to delete ${filePath}.`);
        rejects(new Error(JSON.stringify({ [`failed to delete`]: filePath })));
      } else {

        console.log(`Deleted ${filePath}.`);
        resolve(filePath);

      }

    });
  });
};

const deleteTheseFiles = function (filesPathsList) {
  return Promise.all(
    filesPathsList.map((filePath) => {
      return deleteThisFile(filePath);
    })
  );
};

const readFromFile = function (fileToBeRead, options) {
  options = Object.assign(
    {
      encoding: "utf-8",
    },
    options
  );

  return new Promise((resolve, rejects) => {

    fs.readFile(path.join(__dirname, `/${fileToBeRead}`), options.encoding, (error, dataFromFile) => {
      if (error) {

        console.error(`Failed to read ${fileToBeRead}.`);
        rejects(error);
      } else {

        console.log(`Read ${fileToBeRead} sucessfully.`);
        resolve(dataFromFile);
      }
    });
  });
};

const writeToFile = function (fileName, Data, options) {
  options = Object.assign(
    {
      encoding: "utf-8",
      flag: "w",
    },
    options
  );
  return new Promise((resolve, rejects) => {
    fs.writeFile(path.join(__dirname, `/${fileName}`), Data, options, (error) => {
      if (error) {
        console.error(`Failed to write in ${fileName}!`);
        rejects(error);
      } else {
        console.log(`Sucessfully wrote in ${path.join(__dirname, `/${fileName}`)}!`);
        resolve()
      }

    });
  });

};

/**
 * problem2 uses promises and fs module's asynchronous functions, to do the following:
 * 1. Read the given file lipsum.txt
 * 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
 * 3. Read the new file and convert it to lowercase. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
 * 4. Read the new files, sort the content, and write it out to a new file. Store the name of the new file in filenames.txt
 * 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
 * @param {String} fileToBeRead 
 * @param {String} fileToStoreFileNames 
 */
const problem2 = function (fileToBeRead, fileToStoreFileNames) {
  if (fileToBeRead === undefined || fileToStoreFileNames === undefined ||
    typeof fileToBeRead !== "string" || typeof fileToStoreFileNames !== "string" ||
    fileToBeRead === "" || fileToStoreFileNames === "" ||
    fileToStoreFileNames === " " || fileToBeRead === " "
  ) {
    console.log("Please Provide valid arguments");
  }


  readFromFile(fileToBeRead)
    .then((retunedData) => {

      let uppercaseData = retunedData.toUpperCase();
      return writeToFile("Uppercase.txt", JSON.stringify(uppercaseData));
    })
    .then(() => {
      return writeToFile(fileToStoreFileNames, "Uppercase.txt\n");
    })
    .then(() => {
      return readFromFile("Uppercase.txt");
    })
    .then((retunedData) => {

      let lowercaseSentences = retunedData.toLowerCase()
        .split("\n")
        .filter((sentence) => {
          return sentence !== "";
        });

      return writeToFile("lowercaseSentences.txt", JSON.stringify(lowercaseSentences));
    })
    .then(() => {
      return writeToFile(fileToStoreFileNames, "lowercaseSentences.txt\n", { flag: "a" });
    })
    .then(() => {
      return readFromFile("lowercaseSentences.txt");
    })
    .then((retunedData) => {
      let sortedSentences = JSON.stringify(JSON.parse(retunedData)
        .sort());

      return writeToFile("sortedSentences.txt", JSON.stringify(sortedSentences));
    })
    .then(() => {
      return writeToFile(fileToStoreFileNames, "sortedSentences.txt\n", { flag: "a" });
    })
    .then(() => {
      return readFromFile(fileToStoreFileNames);
    })
    .then((retunedData) => {
      let listOfFilePathToBeDeleted = retunedData.trim()
        .split("\n")
        .map((filename) => {
          return path.join(__dirname, filename);
        });
        
      return deleteTheseFiles(listOfFilePathToBeDeleted);
    })
    .catch((error) => { 
      console.error(error) 
    });

}



module.exports = problem2;
