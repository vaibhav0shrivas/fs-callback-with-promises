const problem1 = require("../problem1.cjs");

console.log("test problem1()------------------------------------------");
problem1();

console.log("test problem1('temp')------------------------------------------");
problem1("temp");

console.log("test problem1(2)------------------------------------------");
problem1(2);


console.log("test problem1('blah',2)------------------------------------------");
problem1("blah", 5);
