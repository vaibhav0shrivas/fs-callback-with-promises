
const crypto = require("crypto");
const fs = require("fs");
const path = require("path");


const getRandomObject = function () {
  let bufferObject = Buffer.alloc(10);

  return new Promise((resolve, rejects) => {

    crypto.randomFill(bufferObject, 5, (error, bufferObject) => {

      if (error) {

        rejects(new Error(JSON.stringify({ ["Failed to create random buffer object."]: error })));
      } else {

        console.log("Random buffer object created sucessfully.");
        resolve(bufferObject);
      }
    });
  });
};

const createJSONfile = function (bufferObject, fileNameWithPath) {
  return new Promise((resolve, rejects) => {

    fs.writeFile(fileNameWithPath, bufferObject.toString(), (error) => {

      if (error) {

        rejects(new Error(`Failed to write to ${fileNameWithPath}`));
      } else {

        console.log(`Sucessfully Created File ${fileNameWithPath}`);
        resolve(fileNameWithPath);
      }

    });

  });

};

const createNJSONFiles = function (bufferObject, numberOfFIles, directoryPath) {
  let fileNamesWithPathList = [...Array(numberOfFIles).keys()].map((filenumer) => {
    return path.join(directoryPath, `/${filenumer}.json`);
  });

  return Promise.all(fileNamesWithPathList.map((fileNameWithPath) => {
    return createJSONfile(bufferObject, fileNameWithPath)
  })
  );

};

const deleteThisFile = function (filePath) {
  return new Promise((resolve, rejects) => {

    fs.unlink(filePath, (error) => {

      if (error) {

        console.error(`Failed to delete ${filePath}.`);
        rejects(new Error(JSON.stringify({ [`failed to delete`]: filePath })));
      } else {

        console.log(`Deleted ${filePath}.`);
        resolve(filePath);
      }

    });
  });
};

const deleteTheseFiles = function (filesPathsList) {

  return Promise.all(filesPathsList.map((filePath) => {
    return deleteThisFile(filePath);
  })
  );

}

const createDirectoryWithName = function (folderName) {
  const directoryForJSONFilesPath = path.join(__dirname, `/${folderName}/`);

  return new Promise((resolve, rejects) => {
    fs.mkdir(directoryForJSONFilesPath, { recursive: true }, (error) => {
      if (error) {

        rejects(new Error(JSON.stringify({ ["Directory creation failed!"]: error })))
      } else {

        console.log(`Directory ${folderName} created sucessfully!`);
        resolve();
      }
    });

  });

};

/**
*problem1 uses promises and the fs module's asynchronous functions, do the following:
* 1. Create a directory of random JSON files.
* 2. Delete those files simultaneously.
* @param {string} folderName Folder name in which files will be created.
* @param {number} numberOfFIles Number of files to be created.
*/
const problem1 = function (folderName, numberOfFIles) {
  if (folderName === undefined || numberOfFIles === undefined
    || Number.isInteger(numberOfFIles) == false || numberOfFIles < 0) {

    console.error("Please Provide valid arguments");
    return;
  }

  createDirectoryWithName(folderName)
    .then(() => {

      return getRandomObject();
      
    })
    .then((randomObject) => {

      return createNJSONFiles(randomObject, numberOfFIles, path.join(__dirname + `/${folderName}/`));

    })
    .then((resultOfCreateNJSONFiles) => {

      return deleteTheseFiles(resultOfCreateNJSONFiles);

    })
    .catch((error) => { 
      console.error(error) 
    });

};


module.exports = problem1;
