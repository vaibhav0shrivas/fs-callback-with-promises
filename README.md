# fs-callback-with-promises Project

### A simple fs-callback project, which makes use of Node.js's fs asynchronous functions to create, read, write, and delete directory and files.

## Directory Structure & Problem Statement

#### Directory Structure

      Folder structure:
        ├── problem1.js
        ├── problem2.js
        └── test
            ├── testProblem1.js
            └── testProblem2.js

#### Problem 1
Using Promises and the fs module's asynchronous functions, do the following:
1. Create a directory of random JSON files.
2. Delete those files simultaneously.


#### Problem 2

Using Promises and the fs module's asynchronous functions, do the following:

1. Read the given file lipsum.txt
2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
3. Read the new file and convert it to lowercase. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
4. Read the new files, sort the content, and write it out to a new file. Store the name of the new file in filenames.txt
5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
